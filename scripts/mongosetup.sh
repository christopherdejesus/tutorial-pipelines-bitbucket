#!/bin/bash
echo "Waiting for startup.."
sleep 5
echo "Started.."
mongo --host mongodb-primary:27017 <<EOF
var cfg = {
    "_id": "mongo-rs",
    "version": 1,
    "members": [
        {
            "_id": 0,
            "host": "mongodb-primary:27017",
            "priority": 2
        },
        {
            "_id": 1,
            "host": "mongodb-secondary:27017",
            "priority": 1
        },
        {
            "_id": 2,
            "host": "mongodb-arbiter:27017",
            "arbiterOnly": true,
            "priority": 0
        }
    ]
};
rs.initiate(cfg, { force: true });
EOF

tail -f /dev/null
